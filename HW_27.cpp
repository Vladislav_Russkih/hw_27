﻿#include <iostream>
#include <new>

using namespace std;

template <typename T>
class STACK
{
private:
	T* stack;
	int size;

public:
	STACK()
	{
		stack = nullptr;
		size = 0;
	}

	void Push(T newElement)
	{
		T* tmp;
		try
		{
			tmp = stack;
			stack = new T[size + 1];
			size++;

			for (int i = 0; i < size - 1; i++)
				stack[i] = tmp[i];

			stack[size - 1] = newElement;

				if (size > 1)
					delete[] tmp;
		}
		catch (bad_alloc e)
		{
			cout << e.what() << endl;
		}
	}


	int Pop()
	{
		if (size == 0)
			return 0;
		size --;
		return stack[size];
	}

	~STACK()
	{
		if (size > 0)
			delete[] stack;
	}

	bool ChangeEmpty()
	{
		return size;
	}

	void Print()
	{
		T* p;

		p = stack;

		cout << "Stack: " << endl;
		if (size == 0)
			cout << "is empty." << endl;

		for (int i = 0; i < size; i++)
		{
			cout << "Item[" << i << "] = " << *p << endl;
			p++;
		}
		cout << endl;
	}
};

int main()
{
	STACK <int> st1;

	st1.Print();

	st1.Push(6);
	st1.Push(2);
	st1.Push(4);

	st1.Print();

	st1.Pop();

	st1.Print();

	st1.Pop();

	st1.Print();

	st1.Pop();

	st1.Print();

	st1.Push(9);
	st1.Push(1);
	st1.Push(7);

	st1.Print();
};